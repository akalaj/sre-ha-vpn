# Building an HA VPN using Site Reliability Engineering

This repository is dedicated to prototyping an HA VPN that exists between several cloud providers. The purpose of this code is show how the implementation and support of an HA VPN can be done using DevOps and SRE principles.

| Project Lead | Description |
| - | - |
| Alexander Kalaj | Project lead responsible for the content of this repository |

# Overview

The code in this respository implements the following setup:

![Arch Diagram](https://cloud.google.com/static/network-connectivity/docs/vpn/images/build-ha-vpn-connections-google-cloud-aws.svg)